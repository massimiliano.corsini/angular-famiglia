import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-figlio',
  templateUrl: './figlio.component.html',
  styleUrls: ['./figlio.component.css']
})
export class FiglioComponent {
  @Input() numeriRomani: string[] = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X'];

  isNumeroPari(numeroRomano: string): boolean {
    const numeriRomani = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X'];
    const indice = numeriRomani.indexOf(numeroRomano);
    return indice !== -1 && (indice + 1) % 2 === 0;
  }
}
