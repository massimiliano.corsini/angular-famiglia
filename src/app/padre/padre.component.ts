import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent {
@Input() numeriRomani: string[] = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X'];

isNumeroDispari(numeroRomano: string): boolean {
  const numeriRomani = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X'];
  const indice = numeriRomani.indexOf(numeroRomano);
  return indice !== -1 && (indice + 1) % 2 !== 0;}

}
