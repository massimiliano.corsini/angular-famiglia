Creare un'applicazione Angular chiamata Famiglia.
All'interno dell'App-component crea un array di numeri romani (fino al dieci).
Crea al suo interno un componente chiamato FIGLIO e passagli l'array appena creato, stampando dentro un div solo i numeri romani pari.
Aggiungi dentro il componente figlio, un componente chiamato PADRE.
Passa al padre del componente PADRE un array di numeri romani dispari (fino al 9).
